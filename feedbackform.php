<!DOCTYPE html>
<!--[if IE 9]>
<html class="ie ie9" lang="en-US">
<![endif]-->
<html lang="en-US">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<title></title>
<link rel="stylesheet" href="assets/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/css/simple-line-icons.css">
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/animate.css">
<link rel="stylesheet" href="assets/css/slidebars.css">
<link rel="stylesheet" href="style.css">

</head>
<body id="fun-fact" onscrollstart="return false;">
<style type="text/css">
 BODY {background:none transparent;}
</style>


<div class="contact-form">

<form action="feedback.php" onsubmit="return onMainSubmit();"  enctype="multipart/form-data" method="post" class="form-group">
<div class="row">
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" name="first-name" id="first-name" placeholder="First Name (required)">
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" name="last-name" id="last-name" placeholder="Last Name (required)">
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="email" name="email" id="email" placeholder="Email (required)">
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" name="phone" id="phone" placeholder="Phone">
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" name="subject" id="subject" placeholder="Subject (required)" >
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" name="position" id="position" placeholder="Company and Position (required)" >
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <textarea name="description" id="description" cols="40" placeholder="Description (required)" maxlength="3000"></textarea>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <textarea type="text" name="bio" id="bio" placeholder="Bio (required)" cols="35" maxlength="3000"></textarea>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <select class="select" id="category" name="category">
            <option value="" selected="selected" disabled>Category(required)</option>
            <option value="Cloud">Cloud</option>
            <option value="Architecture">Architecture</option>
            <option value="Containers">Containers</option>
            <option value="Security">Security</option>
            <option value="Innovations">Innovations</option>
            <option value="DevOps Practice">DevOps Practice</option>
            <option value="Automation">Automation</option>
            <option value="Continuous Delivery">Continuous Delivery</option>
            <option value="Others">Others</option>
        </select>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <select class="select" name="country" id="country" >
            <option value="" selected="selected" disabled>Country (required)</option>
            <option value="Afghanistan">Afghanistan</option>
            <option value="Aland Islands">Aland Islands</option>
            <option value="Albania">Albania</option>
            <option value="Algeria">Algeria</option>
            <option value="American Samoa">American Samoa</option>
            <option value="Andorra">Andorra</option>
            <option value="Angola">Angola</option>
            <option value="Anguilla">Anguilla</option>
            <option value="Antarctica">Antarctica</option>
            <option value="Antigua and Barbuda">Antigua and Barbuda</option>
            <option value="Argentina">Argentina</option>
            <option value="Armenia">Armenia</option>
            <option value="Aruba">Aruba</option>
            <option value="Australia">Australia</option>
            <option value="Austria">Austria</option>
            <option value="Azerbaijan">Azerbaijan</option>
            <option value="Bahamas">Bahamas</option>
            <option value="Bahrain">Bahrain</option>
            <option value="Bangladesh">Bangladesh</option>
            <option value="Barbados">Barbados</option>
            <option value="Belarus">Belarus</option>
            <option value="Belgium">Belgium</option>
            <option value="Belize">Belize</option>
            <option value="Benin">Benin</option>
            <option value="Bermuda">Bermuda</option>
            <option value="Bhutan">Bhutan</option>
            <option value="Bolivia">Bolivia</option>
            <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
            <option value="Botswana">Botswana</option>
            <option value="Bouvet Island">Bouvet Island</option>
            <option value="Brazil">Brazil</option>
            <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
            <option value="Brunei Darussalam">Brunei Darussalam</option>
            <option value="Bulgaria">Bulgaria</option>
            <option value="Burkina Faso">Burkina Faso</option>
            <option value="Burundi">Burundi</option>
            <option value="Cambodia">Cambodia</option>
            <option value="Cameroon">Cameroon</option>
            <option value="Canada">Canada</option>
            <option value="Cape Verde">Cape Verde</option>
            <option value="Cayman Islands">Cayman Islands</option>
            <option value="Central African Republic">Central African Republic</option>
            <option value="Chad">Chad</option>
            <option value="Chile">Chile</option>
            <option value="China">China</option>
            <option value="Christmas Island">Christmas Island</option>
            <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
            <option value="Colombia">Colombia</option>
            <option value="Comoros">Comoros</option>
            <option value="Congo">Congo</option>
            <option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option>
            <option value="Cook Islands">Cook Islands</option>
            <option value="Costa Rica">Costa Rica</option>
            <option value="Cote D'ivoire">Cote D'ivoire</option>
            <option value="Croatia">Croatia</option>
            <option value="Cuba">Cuba</option>
            <option value="Cyprus">Cyprus</option>
            <option value="Czech Republic">Czech Republic</option>
            <option value="Denmark">Denmark</option>
            <option value="Djibouti">Djibouti</option>
            <option value="Dominica">Dominica</option>
            <option value="Dominican Republic">Dominican Republic</option>
            <option value="Ecuador">Ecuador</option>
            <option value="Egypt">Egypt</option>
            <option value="El Salvador">El Salvador</option>
            <option value="Equatorial Guinea">Equatorial Guinea</option>
            <option value="Eritrea">Eritrea</option>
            <option value="Estonia">Estonia</option>
            <option value="Ethiopia">Ethiopia</option>
            <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
            <option value="Faroe Islands">Faroe Islands</option>
            <option value="Fiji">Fiji</option>
            <option value="Finland">Finland</option>
            <option value="France">France</option>
            <option value="French Guiana">French Guiana</option>
            <option value="French Polynesia">French Polynesia</option>
            <option value="French Southern Territories">French Southern Territories</option>
            <option value="Gabon">Gabon</option>
            <option value="Gambia">Gambia</option>
            <option value="Georgia">Georgia</option>
            <option value="Germany">Germany</option>
            <option value="Ghana">Ghana</option>
            <option value="Gibraltar">Gibraltar</option>
            <option value="Greece">Greece</option>
            <option value="Greenland">Greenland</option>
            <option value="Grenada">Grenada</option>
            <option value="Guadeloupe">Guadeloupe</option>
            <option value="Guam">Guam</option>
            <option value="Guatemala">Guatemala</option>
            <option value="Guernsey">Guernsey</option>
            <option value="Guinea">Guinea</option>
            <option value="Guinea-bissau">Guinea-bissau</option>
            <option value="Guyana">Guyana</option>
            <option value="Haiti">Haiti</option>
            <option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option>
            <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
            <option value="Honduras">Honduras</option>
            <option value="Hong Kong">Hong Kong</option>
            <option value="Hungary">Hungary</option>
            <option value="Iceland">Iceland</option>
            <option value="India">India</option>
            <option value="Indonesia">Indonesia</option>
            <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
            <option value="Iraq">Iraq</option>
            <option value="Ireland">Ireland</option>
            <option value="Isle of Man">Isle of Man</option>
            <option value="Israel">Israel</option>
            <option value="Italy">Italy</option>
            <option value="Jamaica">Jamaica</option>
            <option value="Japan">Japan</option>
            <option value="Jersey">Jersey</option>
            <option value="Jordan">Jordan</option>
            <option value="Kazakhstan">Kazakhstan</option>
            <option value="Kenya">Kenya</option>
            <option value="Kiribati">Kiribati</option>
            <option value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option>
            <option value="Korea, Republic of">Korea, Republic of</option>
            <option value="Kuwait">Kuwait</option>
            <option value="Kyrgyzstan">Kyrgyzstan</option>
            <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
            <option value="Latvia">Latvia</option>
            <option value="Lebanon">Lebanon</option>
            <option value="Lesotho">Lesotho</option>
            <option value="Liberia">Liberia</option>
            <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
            <option value="Liechtenstein">Liechtenstein</option>
            <option value="Lithuania">Lithuania</option>
            <option value="Luxembourg">Luxembourg</option>
            <option value="Macao">Macao</option>
            <option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option>
            <option value="Madagascar">Madagascar</option>
            <option value="Malawi">Malawi</option>
            <option value="Malaysia">Malaysia</option>
            <option value="Maldives">Maldives</option>
            <option value="Mali">Mali</option>
            <option value="Malta">Malta</option>
            <option value="Marshall Islands">Marshall Islands</option>
            <option value="Martinique">Martinique</option>
            <option value="Mauritania">Mauritania</option>
            <option value="Mauritius">Mauritius</option>
            <option value="Mayotte">Mayotte</option>
            <option value="Mexico">Mexico</option>
            <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
            <option value="Moldova, Republic of">Moldova, Republic of</option>
            <option value="Monaco">Monaco</option>
            <option value="Mongolia">Mongolia</option>
            <option value="Montenegro">Montenegro</option>
            <option value="Montserrat">Montserrat</option>
            <option value="Morocco">Morocco</option>
            <option value="Mozambique">Mozambique</option>
            <option value="Myanmar">Myanmar</option>
            <option value="Namibia">Namibia</option>
            <option value="Nauru">Nauru</option>
            <option value="Nepal">Nepal</option>
            <option value="Netherlands">Netherlands</option>
            <option value="Netherlands Antilles">Netherlands Antilles</option>
            <option value="New Caledonia">New Caledonia</option>
            <option value="New Zealand">New Zealand</option>
            <option value="Nicaragua">Nicaragua</option>
            <option value="Niger">Niger</option>
            <option value="Nigeria">Nigeria</option>
            <option value="Niue">Niue</option>
            <option value="Norfolk Island">Norfolk Island</option>
            <option value="Northern Mariana Islands">Northern Mariana Islands</option>
            <option value="Norway">Norway</option>
            <option value="Oman">Oman</option>
            <option value="Pakistan">Pakistan</option>
            <option value="Palau">Palau</option>
            <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
            <option value="Panama">Panama</option>
            <option value="Papua New Guinea">Papua New Guinea</option>
            <option value="Paraguay">Paraguay</option>
            <option value="Peru">Peru</option>
            <option value="Philippines">Philippines</option>
            <option value="Pitcairn">Pitcairn</option>
            <option value="Poland">Poland</option>
            <option value="Portugal">Portugal</option>
            <option value="Puerto Rico">Puerto Rico</option>
            <option value="Qatar">Qatar</option>
            <option value="Reunion">Reunion</option>
            <option value="Romania">Romania</option>
            <option value="Russian Federation">Russian Federation</option>
            <option value="Rwanda">Rwanda</option>
            <option value="Saint Helena">Saint Helena</option>
            <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
            <option value="Saint Lucia">Saint Lucia</option>
            <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
            <option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option>
            <option value="Samoa">Samoa</option>
            <option value="San Marino">San Marino</option>
            <option value="Sao Tome and Principe">Sao Tome and Principe</option>
            <option value="Saudi Arabia">Saudi Arabia</option>
            <option value="Senegal">Senegal</option>
            <option value="Serbia">Serbia</option>
            <option value="Seychelles">Seychelles</option>
            <option value="Sierra Leone">Sierra Leone</option>
            <option value="Singapore">Singapore</option>
            <option value="Slovakia">Slovakia</option>
            <option value="Slovenia">Slovenia</option>
            <option value="Solomon Islands">Solomon Islands</option>
            <option value="Somalia">Somalia</option>
            <option value="South Africa">South Africa</option>
            <option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option>
            <option value="Spain">Spain</option>
            <option value="Sri Lanka">Sri Lanka</option>
            <option value="Sudan">Sudan</option>
            <option value="Suriname">Suriname</option>
            <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
            <option value="Swaziland">Swaziland</option>
            <option value="Sweden">Sweden</option>
            <option value="Switzerland">Switzerland</option>
            <option value="Syrian Arab Republic">Syrian Arab Republic</option>
            <option value="Taiwan, Province of China">Taiwan, Province of China</option>
            <option value="Tajikistan">Tajikistan</option>
            <option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
            <option value="Thailand">Thailand</option>
            <option value="Timor-leste">Timor-leste</option>
            <option value="Togo">Togo</option>
            <option value="Tokelau">Tokelau</option>
            <option value="Tonga">Tonga</option>
            <option value="Trinidad and Tobago">Trinidad and Tobago</option>
            <option value="Tunisia">Tunisia</option>
            <option value="Turkey">Turkey</option>
            <option value="Turkmenistan">Turkmenistan</option>
            <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
            <option value="Tuvalu">Tuvalu</option>
            <option value="Uganda">Uganda</option>
            <option value="Ukraine">Ukraine</option>
            <option value="United Arab Emirates">United Arab Emirates</option>
            <option value="United Kingdom">United Kingdom</option>
            <option value="United States">United States</option>
            <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
            <option value="Uruguay">Uruguay</option>
            <option value="Uzbekistan">Uzbekistan</option>
            <option value="Vanuatu">Vanuatu</option>
            <option value="Venezuela">Venezuela</option>
            <option value="Viet Nam">Viet Nam</option>
            <option value="Virgin Islands, British">Virgin Islands, British</option>
            <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
            <option value="Wallis and Futuna">Wallis and Futuna</option>
            <option value="Western Sahara">Western Sahara</option>
            <option value="Yemen">Yemen</option>
            <option value="Zambia">Zambia</option>
            <option value="Zimbabwe">Zimbabwe</option>
        </select>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <select class="select" id="duration" name="duration" >
            <option value="" selected="selected" disabled>Duration (required)</option>
            <option value="45 minutes">45 minutes</option>
            <option value="30 minutes">30 minutes</option>
            <option value="15 minutes">15 minutes</option>
        </select>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input class="custom-file-input" type="file" name="file-photo" id="file-photo">
    </div>
    <div class="col-md-12 col-sm-6 col-xs-12">
        <textarea name="message" id="message" cols="40" placeholder="Additional Information"></textarea>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div id="example3"></div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-6">
        <a id="сancel_form" href="#" class="t-btn t-btn-light btn-sbm-tlk">Cancel</a>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-6">
        <input name="submit" type="submit" id="submit" tabindex="5" value="Submit Talk" class="comment-submit t-btn t-btn-primary">
    </div>


</div>
</form> <!-- .form-group -->
</div> <!-- .contact-form -->

<script type="text/javascript" src="assets/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/appear.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.countTo.js"></script>
<script type="text/javascript" src="assets/js/jquery.scrollTo.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.localScroll.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.viewport.mini.js"></script>
<script type="text/javascript" src="assets/js/skrollr.js"></script>
<script type="text/javascript" src="assets/js/smoothscroll.min.js"></script>
<script type="text/javascript" src="assets/js/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" src="assets/js/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="assets/js/slidebars.min.js"></script>
<script type="text/javascript" src="assets/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="assets/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="assets/js/main.js?test=45663424365"></script>

<script>

var is_24444322test2 = -1
var start_options = ""

$(window).load(function() {

    start_options = $("#duration").html();

    parent.iframeLoaded($(".contact-form").position().top + 10 + $(".contact-form").height());
});

$(window).resize(function() {
    parent.iframeLoaded($(".contact-form").position().top + 10 + $(".contact-form").height());
});

$("#сancel_form").click(function() {
    parent.revertForm();
});

//$("#category").change(function(e){
//    if ($(this).val() == "Workshop"){
//        $("#duration").html("<option value='3.5 hours'>3.5 hours</pbtion>");
//    } else {
//        $("#duration").html(start_options);
//    }
//
//    return true;
//});

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validateBio(str_bio){
    return ((str_bio.length > 40) && (str_bio.length < 3000));
}

function onMainSubmit(){

    
    var correct_bg = "rgba(255, 255, 255, 0.1)";
    var incorrect_bg = "rgba(255, 0, 0, 0.4)";
    
    var email = $("#email").val().trim();
    var fname = $("#first-name").val().trim();
    var lname = $("#last-name").val().trim();
    
    var bio = $("#bio").val().trim();
    var description = $("#description").val().trim();
    
    var category = $("#category").prop('selectedIndex');
    var country = $("#country").prop('selectedIndex');
    var duration = $("#duration").prop('selectedIndex');
    
    var subject = $("#subject").val().trim();
    var position = $("#position").val().trim();
    
    err = "";
    
    /* Validate first name */
    $("#first-name").css({"background-color": ((fname.length > 0) ? correct_bg : incorrect_bg)});
    
    if (fname.length <= 0){
        err += "Please provide First Name\n";
    }
    
    /* Validate last name */
    $("#last-name").css({"background-color": ((lname.length > 0) ? correct_bg : incorrect_bg)});
    
    if (lname.length <= 0){
        err += "Please provide Last Name\n";
    }
    
    /* Validate email */
    $("#email").css({"background-color": (validateEmail(email) ? correct_bg : incorrect_bg)});
    
    if (!validateEmail(email)){
        err += "Please provide correct Email\n";
    }
    
    /* Validate bio */
    $("#bio").css({"background-color": (validateBio(bio) ? correct_bg : incorrect_bg)});
    
    if (!validateBio(bio)){
        err += "Bio should have from 40 to 3000 characters\n";
    }
    
    /* Validate description */
    $("#description").css({"background-color": (validateBio(description) ? correct_bg : incorrect_bg)});
    
    if (!validateBio(description)){
        err += "Description should have from 40 to 3000 characters\n";
    }
    
    
    /* Validate others */
    $("#category").css({"background-color": ((category != 0) ? correct_bg : incorrect_bg)});
    
    if (category == 0){
        err += "Please select Category\n";
    }
    
    $("#country").css({"background-color": ((country != 0) ? correct_bg : incorrect_bg)});
    
    if (country == 0){
        err += "Please select Country\n";
    }
    
    if ($("#category").val() != "Workshop"){
        $("#duration").css({"background-color": ((duration != 0) ? correct_bg : incorrect_bg)});

        if (duration == 0){
            err += "Please select Duration\n";
        }
    }

    $("#subject").css({"background-color": ((subject.length > 0) ? correct_bg : incorrect_bg)});
    
    if (subject.length <= 0){
        err += "Please provide Subject\n";
    }
    
    $("#position").css({"background-color": ((position.length > 0) ? correct_bg : incorrect_bg)});
    
    if (position.length <= 0){
        err += "Please provide Company and Position\n";
    }

    
    /* Validate I'm not a robot */
    if (is_24444322test2 == -1){
        err += "\nPlease check the \"I'm not a robot\" checkbox\n";
    }

    
    if (err != ""){
        alert(err);
        return false;
    } else {
        return true;
    }
    

}

var verifyCallback = function(response) {
    is_24444322test2 = response;
};

var expiredcallback = function(response) {
    is_24444322test2 = -1;
};

var onloadCallback = function() {
    grecaptcha.render('example3', {
                      'sitekey' : '6LdoeAoUAAAAAEIzeb5tpfw9S3s8BrtO1R7LCX2Q',
                      'callback' : verifyCallback,
                      'expired-callback' : expiredcallback,
                      'theme' : 'dark'
                      });
};
</script>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>


</body>
</html>
