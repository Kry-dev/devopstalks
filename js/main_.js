var devopsTalks = {

    parallaxSkrollr: function () {
        skrollr.init({
            forceHeight: false,
            mobileCheck: function () {
                return false;
            }
        });
    },


    navigationScroll: function () {

        $(".local-scroll").localScroll({
            target: "body",
            duration: 1000,
            offset: -100,
            easing: "easeInOutExpo"
        });

        var sections = $(".home-widget, .page-widget, .page-top");
        var menu_links = $(".main-menu li a");

        $(window).scroll(function () {

            sections.filter(":in-viewport:first").each(function () {
                var active_section = $(this);
                var active_link = $('.main-menu li a[href="index.html#' + active_section.attr("id") + '"]');
                menu_links.removeClass("active");
                active_link.addClass("active");
            });

        });

    },


    headerFixed: function () {

        $(document).on('scroll', function () {
            // if the scroll distance is greater than 100px
            if ($(window).scrollTop() > 100) {
                // do something
                $('#header').addClass('scrolled-header');
            }
            else {
                $('#header').removeClass('scrolled-header');
            }
        });

    },


    isotopeFilter: function () {

        var filter = $('.portfolio-filter'),
            portfolioGrid = $('.portfolio-container');

        portfolioGrid.imagesLoaded(function () {
            portfolioGrid.isotope({
                itemSelector: '.portfolio-post',
                layoutMode: 'fitRows',
            });
        });

        var filterFns = {
            // show if number is greater than 50
            numberGreaterThan50: function () {
                var number = $(this).find('.number').text();
                return parseInt(number, 10) > 50;
            },
            // show if name ends with -ium
            ium: function () {
                var name = $(this).find('.name').text();
                return name.match(/ium$/);
            }
        };

        filter.on('click', 'a', function () {
            var filterValue = $(this).attr('data-filter');
            // use filterFn if matches value
            filterValue = filterFns[filterValue] || filterValue;
            portfolioGrid.isotope({filter: filterValue});
            return false;
        });

        filter.each(function (i, buttonGroup) {
            var $buttonGroup = $(buttonGroup);
            $buttonGroup.on('click', 'a', function () {
                $buttonGroup.find('.active').removeClass('active');
                $(this).addClass('active');
            });
        });

    },


    fanFacts: function () {
        $(".count-number").appear(function () {
            datacount = $(this).attr('data-count');
            $(this).find('.count-focus').delay(6000).countTo({
                from: 10,
                to: datacount,
                speed: 3000,
                refreshInterval: 50,
            });
        });
    },


    revSlider: function () {
        $('.fullscreenbanner').revolution({
            delay: 10000,
            startheight: 650,
            startwidth: 1120,

            hideThumbs: 200,
            hideTimerBar: "on",

            thumbWidth: 100,							// Thumb With and Height and Amount (only if navigation Type set to thumb !)
            thumbHeight: 50,
            thumbAmount: 5,

            navigationType: "bullet",					//bullet, thumb, none, both		(No Thumbs In FullWidth Version !)
            navigationArrows: "verticalcentered",		//nexttobullets, verticalcentered, none
            navigationStyle: "round",				//round,square,navbar

            touchenabled: "on",						// Enable Swipe Function : on/off
            onHoverStop: "on",						// Stop Banner Timet at Hover on Slide on/off

            navOffsetHorizontal: 0,
            navOffsetVertical: 0,

            stopAtSlide: -1,
            stopAfterLoops: -1,

            shadow: 0,								//0 = no Shadow, 1,2,3 = 3 Different Art of Shadows  (No Shadow in Fullwidth Version !)
            fullScreen: "on"							// Turns On or Off the Fullwidth Image Centering in FullWidth Modus
        });
    },


    scrollAnimation: function () {
        $('.animated').each(function () {
            $(this).appear(function () {
                var element = $(this);
                var animation = element.attr('data-animation');
                if (!element.hasClass('visible')) {
                    var animationDelay = element.attr('data-animation-delay');
                    setTimeout(function () {
                            element.addClass(animation + ' visible');
                        }, animationDelay
                    );
                }
            });

        });

    },


    blogMasonry: function () {

        var $blogContainer = $('.blog-masonry');

        $blogContainer.imagesLoaded(function () {

            $blogContainer.isotope({
                itemSelector: '.post-masonry'
            });

        });

    },


    bxSlider: function () {

        var $bxWrapper = $('.bxslider');

        $bxWrapper.bxSlider({
            adaptiveHeight: true,
            // mode: 'fade',
            minSlides: 1,
            maxSlides: 1,
            onSliderLoad: function () {
                $('.bx-controls-direction').hide();
                $('.bx-wrapper').hover(
                    function () {
                        $('.bx-controls-direction').fadeIn(300);
                    },
                    function () {
                        $('.bx-controls-direction').fadeOut(300);
                    }
                );
            },
        });
    },


    clientOwl: function () {

        var $clients = $('.clients-wrapper');

        $clients.owlCarousel({
            navigation: false,
            autoPlay: true,
            pagination: false,
            items: 6,
            itemsDesktop: [1200, 5],
            itemsDesktopSmall: [992, 4],
            itemsTablet: [768, 3],
            itemsMobile: [480, 2]
        });

    },


    slideBars: function () {

        $.slidebars();

    },

    mobileNav: function () {

        $(".slidebar-menu").on("click", ".has-submenu", function (e) {

            $(this).parent().toggleClass("active")
                .children(".sub-menu").toggleClass("open");

            return false;

        });
    },


    scrollTop: function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('.go-top').fadeIn();
            } else {
                $('.go-top').fadeOut();
            }
        });

        // scroll body to 0px on click
        $('.go-top a').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
    },
    countDown: function() {
        $('#clock').countdown('2017/05/11 09:00:00').on('update.countdown', function(e) {
            var $this = $(this).html(e.strftime(''
                + '<div class="col-md-offset-1 col-md-2 col-xs-3"><span>%-w</span> week%!w </div>'
                + '<div class="col-md-2 col-xs-3"><span>%-d</span> day%!d </div>'
                + '<div class="col-md-2 col-xs-3"><span>%H</span> hr </div>'
                + '<div class="col-md-2 col-xs-3"><span>%M</span> min </div>'
                + '<div class="col-md-2 col-xs-3"><span>%S</span> sec </div>'));
        });
    },
    showContact: function () {
        $('#formContainer').hide();
        // scroll body to 0px on click
        $('#show_form').click(function(e){
            e.preventDefault();
            $('#formContainer').slideToggle();
            return false;
        });
        
    },
    googleMaps:  function() {

        $(".map-button").click(function() {
            $("#map-holder").slideToggle(300, function() {
            
        }), $(this).toggleClass("hide-map show-map")
    })
    }    

};

$(document).ready(function() {

    devopsTalks.navigationScroll();
    devopsTalks.revSlider();
    devopsTalks.headerFixed();
    devopsTalks.isotopeFilter();
    devopsTalks.fanFacts();
    devopsTalks.scrollAnimation();
    devopsTalks.blogMasonry();
    devopsTalks.bxSlider();
    devopsTalks.clientOwl();
    devopsTalks.slideBars();
    devopsTalks.parallaxSkrollr();
    devopsTalks.mobileNav();
    devopsTalks.scrollTop();
    devopsTalks.showContact();
    devopsTalks.countDown();
    devopsTalks.googleMaps();

});


$(window).load(function(){

    // Page loader
    $(".loader-wrap .spinner").delay(0).fadeOut();
    $(".loader-wrap").delay(200).fadeOut("slow");


});

