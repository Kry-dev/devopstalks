<?php

    define("CLASSES_PATH", 'inc/');
    
    require_once(CLASSES_PATH . 'functions.php');
    
        
    function sendMailHTMLEx($recipient, $subject, $sender, $body, $a_files_attached){
        // Generate a boundary string
        $semi_rand = md5(time());
        //$mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";
        $mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";
        
        $headers = "From: $sender";
        
        // Add the headers for a file attachment
        $headers .= "\nMIME-Version: 1.0\n" .
        "Content-Type: multipart/mixed;\n" .
        " boundary=\"{$mime_boundary}\"";
        
        // Add a multipart boundary above the plain message
        $message = "This is a multi-part message in MIME format.\n\n";
        
        $message .= "--{$mime_boundary}\n" .
        "Content-Type: text/html; charset=\"UTF-8\"\n" .
        "Content-Transfer-Encoding: 7bit\n\n" .
        $body . "\n\n";
        
        if (!empty($a_files_attached) && (count($a_files_attached) > 0)){
            for ($i=0; $i<count($a_files_attached); $i++){
                if (file_exists($a_files_attached[$i])){
                    
                    $a_temp = split("/", $a_files_attached[$i]);
                    $fileatt_name  = $a_temp[count($a_temp)-1];
                    
                    $fileatt_type = substr($fileatt_name, strrpos($fileatt_name, '.')+1);
                    
                    // Read the file to be attached ('rb' = read binary)
                    $file = fopen($a_files_attached[$i],'rb');
                    $data = fread($file, filesize($a_files_attached[$i]));
                    fclose($file);
                    
                    // Base64 encode the file data
                    $data = chunk_split(base64_encode($data));
                    
                    $cont_type = mime_content_type($a_files_attached[$i]);
                    
                    // Add file attachment to the message
                    $message .= "--{$mime_boundary}\n" .
                    "Content-Disposition: attachment;\n" .
                    " filename={$fileatt_name};\n" .
                    "Content-Type: $cont_type;\n" .
                    " x-unix-mode=0644;\n" .
                    " name=\"{$fileatt_name}\"\n" .
                    "Content-Transfer-Encoding: base64\n\n" .
                    $data . "\n\n";
                    
                    if ($i == (count($a_files_attached) - 1)){
                        $message .= "--{$mime_boundary}--\n\n\n";
                    }
                }
            }
        }
        
        return mail($recipient, $subject, $message, $headers);
    }

    function tempdir($dir=NULL,$prefix=NULL) {
        $template = "{$prefix}XXXXXX";
        if (($dir) && (is_dir($dir))) { $tmpdir = "--tmpdir=$dir"; }
        else { $tmpdir = '--tmpdir=' . sys_get_temp_dir(); }
        return exec("mktemp -d $tmpdir $template");
    }

    
    /* Script Entry point (get customer form parameters) */
    $a_names = [
        "First Name", "Last Name", "Email", "Phone", "Country", "Position", "Bio", "Description", "Title", "Category", "Duration", "Message"
    ];
    
    $a_fileds = [
        "first-name", "last-name", "email", "phone", "country", "position", "bio", "description", "title", "category", "duration", "message"
    ];
    
    $a_types = array();
    
    for ($i = 0; $i < count($a_names); $i++){
        $a_types[$i] = null;
    }
    
    $a_types[9] = ["Agile", "Automation Tools", "Case studies", "Culture", "Measurement"];
    $a_types[10] = ["45 minutes", "15 minutes"];
    
    $strUserEmail = request_string("email", "", null, null);
    
    $str_body = "";
    
    for ($i = 0; $i < count($a_names); $i++){
        
        $str_value = get_param($a_fileds[$i], null);
        
        //echo "Param : ", $a_fileds[$i], " = ", $str_value;
        
        if (empty($a_types[$i])){
            if (!empty($str_value)){
                $str_value = str_replace("\n", "<br>", $str_value);
                
                $str_body = $str_body . $a_names[$i] . " : " . $str_value . "<br><br>";
            }
        } else if (!empty($str_value) || ($str_value === "0")) {
            $str_body = $str_body  . $a_names[$i] . " : " . $a_types[$i][intval($str_value)] . "<br><br>";
        }
    }
    
    /* Attempt to  */
    $file = basename($_FILES['file-photo']['name']);
    $uploadfile = null;
    $files_to_attach = array();
    
    if (!empty($file)){
        $dir = tempdir();
        
        $uploadfile = $dir . "/" . $file;
        
        if (move_uploaded_file($_FILES['file-photo']['tmp_name'], $uploadfile)) {
            $files_to_attach = array($uploadfile);
        } else {
            die("-1");
        }
    }
    
    
    $int_result = sendMailHTMLEx("Yaroslavkryvda@gmail.com", 'TALKS CONFERENCE, AUSTRALIA, 2017 - Speakers', $strUserEmail, $str_body, $files_to_attach);
    
    header("Location: http://www.devopstalks.com/index.html");
?>
