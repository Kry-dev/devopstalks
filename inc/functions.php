<?php

function request_string($strValue, $strDefault, $intLow, $intHigh) {
    $strValue = stripslashes(get_param($strValue));

    if (strlen($strValue) > 0) {
        $strValue = trim($strValue);

        if (!is_null($intLow)) {
            if (strlen($strValue) < intval($intLow))
                return $strDefault;
        }
        if (!is_null($intHigh)) {
            if (strlen($strValue) > intval($intHigh))
                $strValue = substr($strValue, 0, $intHigh);
            return $strValue;
        }

        return $strValue;
    }

    return $strDefault;
}

function request_integer($strValue, $intDefault, $intLow, $intHigh) {
    $strValue = get_param($strValue);

    $function_ret = $intDefault;
    if (strlen($strValue) > 0) {
        if (is_numeric($strValue)) {
            $function_ret = intval($strValue);
            if (!is_null($intLow))
                if ($function_ret < intval($intLow))
                    $function_ret = $intDefault;
            if (!is_null($intHigh))
                if ($function_ret > intval($intHigh))
                    $function_ret = $intDefault;
        }
    }
    return $function_ret;
}

function get_param($name, $default = NULL) {

    if (isset ($_POST[$name])) {
        return $_POST[$name];
    }
    else {
        if (isset ($_GET[$name])) {
            return $_GET[$name];
        }
        else {
            return $default;
        }
    }
}

?>